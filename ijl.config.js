const pkg = require("./package.json");

module.exports = {
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name.replace("uds-", "")}/${process.env.VERSION || pkg.version}/`,
    },
    module: {
      rules: [
        { parser: { system: false } },
        {
          test: /\.tsx?$/,
          loader: "awesome-typescript-loader",
        },
        {
          test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/,
          loader: "file-loader",
        },
        {
          test: /\.css$/,
          use: ["css-loader"],
        },
        {
          test: /\.s[ac]ss$/i,
          use: ["style-loader", "css-loader", "sass-loader"],
        },
      ],
    },
  },

  apps:{"news":{"version":process.env.VERSION || pkg.version,"name":"news"}},
  apiPath: "./stubs/api",
  navigations: {
    news: "/news",
    orgs: "/orgs",
  },
  config: {
    "news.api.base.url": "http://localhost:3000",
  },
};
