const path = require("path");

const pkg = require(path.resolve("package"));

module.exports = {
  [pkg.name.substring(4)]: {
    version: pkg.version,
  },
};
