const defaultConfig = require("./config");

module.exports = {
  baseUrl: defaultConfig.baseUrl,
  pageTitle: "Новости",
  apps: require("./app"),
  navigations: require("./navigations"),
  config: defaultConfig,
  versionBootstrap: "1.0.0",
};
