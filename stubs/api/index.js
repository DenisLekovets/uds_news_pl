const router = require("express").Router();

router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://89.223.91.151:8080");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

router.use(require("body-parser").json());

const commentDescription = [
  {
    id: 1,
    author: "Виталий Гройсман",
    date: "22 января 2019",
    text:
      "Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные? Непонятно как такое может быть? Никакого обмана!",
    likes: 13,
    isLiked: true,
  },
  {
    id: 2,
    author: "Виталий Гройсман",
    date: "22 января 2019",
    text: "Kek",
    likes: 155,
    isLiked: false,
  },
];

const articles = [
  {
    id: 0,
    title:
      " ГБУ «Жилищник» стало претендентом на миллиардные контракты по капремонту",
    author: "Виталий Гройсман",
    date: "11/22/2019",
    text:
      " Вот так вот" +
      "\n" +
      "> Blockquotes are very handy in email to emulate reply text.\n" +
      "> This line is part of the same quote.",
    ncomments: 5,
    comments: [],
    likes: 10,
    isLiked: false,
    tags: [0, 1, 2],
  },
  {
    id: 1,
    title:
      " ГБУ «Жилищник» стало претендентом на миллиардные контракты по капремонту",
    author: "Виталий Гройсман",
    date: "01/22/2020",
    text:
      " Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные?\n" +
      "                    Непонятно как такое может быть? Никакого обмана! Давайте знакомится, посмотрите видео и если\n" +
      "                    останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24 / 7 и ответить на все вопросы. Мы\n" +
      "                    предложим спортивные, творческие и развивающие кружки и секции для детей и взрослых любого возраста.\n" +
      "                    Количество мест может быть ограничено.\n" +
      "                    Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные?\n" +
      "                    Непонятно как такое может быть? Никакого обмана! Давайте знакомится, посмотрите видео и если\n" +
      "                    останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24 / 7 и ответить на все вопросы. Мы\n" +
      "                    предложим спортивные, творческие и развивающие кружки и секции для детей и взрослых любого возраста.\n" +
      "                    Количество мест может быть ограничено.",
    ncomments: 2,
    comments: commentDescription.slice(),
    likes: 20,
    isLiked: true,
    tags: [1],
  },
  {
    id: 2,
    title:
      " ГБУ «Жилищник» стало претендентом на миллиардные контракты по капремонту",
    author: "Виталий Гройсман",
    date: "01/23/2020",
    text:
      " Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные?\n" +
      "                    Непонятно как такое может быть? Никакого обмана! Давайте знакомится, посмотрите видео и если\n" +
      "                    останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24 / 7 и ответить на все вопросы. Мы\n" +
      "                    предложим спортивные, творческие и развивающие кружки и секции для детей и взрослых любого возраста.\n" +
      "                    Количество мест может быть ограничено.\n" +
      "                    Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные?\n" +
      "                    Непонятно как такое может быть? Никакого обмана! Давайте знакомится, посмотрите видео и если\n" +
      "                    останутся вопросы, позвоните нам, мы готовы принять ваш звонок 24 / 7 и ответить на все вопросы. Мы\n" +
      "                    предложим спортивные, творческие и развивающие кружки и секции для детей и взрослых любого возраста.\n" +
      "                    Количество мест может быть ограничено.",
    ncomments: 3,
    comments: commentDescription.slice(),
    likes: 30,
    isLiked: false,
    tags: [0, 4],
  },
];

const archives = [
  {
    title: "Март 2020",
    month: 3,
    year: 2020,
  },
  {
    title: "Февраль 2019",
    month: 2,
    year: 2020,
  },
  {
    title: "Январь 2020",
    month: 1,
    year: 2020,
  },
  {
    title: "Декабрь 2019",
    month: 12,
    year: 2019,
  },
  {
    title: "Ноябрь 2019",
    month: 11,
    year: 2019,
  },
];

const tags = [
  {
    id: 0,
    text: "тег1",
  },
  {
    id: 1,
    text: "тег2",
  },
  {
    id: 2,
    text: "тег3",
  },
  {
    id: 3,
    text: "тег4",
  },
  {
    id: 4,
    text: "тег5",
  },
];

router.get("/getTags", (req, res) => {
  res.send(tags);
});

router.get("/getNews", (req, res) => {
  res.send(articles);
});

router.get("/getArchives", (req, res) => {
  res.send(archives);
});

router.get("/getComments", (req, res) => {
  const article_id = req.query.id;
  const comments = articles.filter((item) => +item.id === +article_id)[0]
    .comments;
  res.send(comments);
});

router.post("/addComment", (req, res) => {
  const article_id = req.query.id;
  const found_index = articles.findIndex((item) => +item.id === +article_id);
  articles[found_index].comments.push({
    id: articles[found_index].comments.length + 1,
    ...req.body,
    date: "2 мая 2020",
    likes: 0,
    isLiked: false,
  });
  const comments = articles.filter((item) => +item.id === +article_id)[0]
    .comments;
  res.send(comments);
});

router.patch("/editComment", (req, res) => {
  const { articleId, commentId } = req.query;
  const articleIdx = articles.findIndex((x) => +x.id === +articleId);
  const commentIdx = articles[articleIdx].comments.findIndex(
    (x) => +x.id === +commentId
  );
  let likes = articles[articleIdx].comments[commentIdx].likes;
  if (articles[articleIdx].comments[commentIdx].isLiked && !req.body.isLiked) {
    likes -= 1;
  } else if (
    !articles[articleIdx].comments[commentIdx].isLiked &&
    req.body.isLiked
  ) {
    likes += 1;
  }
  articles[articleIdx].comments[commentIdx] = {
    ...articles[articleIdx].comments[commentIdx],
    isLiked: req.body.isLiked,
    likes,
  };
  res.send(articles[articleIdx].comments);
});

router.get("/getArticle", (req, res) => {
  const id = req.query.id;
  res.send(articles.filter((item) => +item.id === +id)[0]);
});

router.patch("/editArticle", (req, res) => {
  const { id } = req.query;
  const articleIdx = articles.findIndex((x) => +x.id === +id);
  let likes = articles[articleIdx].likes;
  if (articles[articleIdx].isLiked && !req.body.isLiked) {
    likes -= 1;
  } else if (!articles[articleIdx].isLiked && req.body.isLiked) {
    likes += 1;
  }
  articles[articleIdx] = {
    ...articles[articleIdx],
    isLiked: req.body.isLiked,
    likes,
  };
  res.send(articles[articleIdx]);
});

router.get("/workflow", require("./session"));

module.exports = router;
