import React from "react";
import ReactDom from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import App from "./App";

// ReactDom.render(<App/>, document.getElementById('app'));

export default () => <App />;

export const mount = (Сomponent) => {
  ReactDom.render(<Сomponent />, document.getElementById("app"));
};

export const unmount = () => {
  ReactDom.unmountComponentAtNode(document.getElementById("app"));
};

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
