import React from "react";
import { mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Provider } from "react-redux";

import moxios from "moxios";
import { act } from "react-dom/test-utils";
import News from "./pages/News";
import { store } from "./redux/store";

import { BrowserRouter } from "react-router-dom";
import Layout from "@uds-news/Layout";

configure({ adapter: new Adapter() });

describe("mount all app", () => {
  beforeEach(() => {
    moxios.install();
    // @ts-ignore
  });

  afterEach(() => moxios.uninstall());

  it("show error on error", async () => {
    const app = mount(
      <Provider store={store}>
        <BrowserRouter>
          <Layout>
            <News />
          </Layout>
        </BrowserRouter>
      </Provider>
    );

    await moxios.wait(jest.fn);

    await act(async () => {
      const request = moxios.requests.mostRecent();
      await request.respondWith({
        status: 404,
      });
    });

    app.update();
    expect(app).toMatchSnapshot();
  });



});
