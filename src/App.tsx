import React from "react";
import "./App.css";
import { Provider } from "react-redux";
import News from "./pages/News";
import history from "./history";
import Layout from "./Layout";
import NewsArticle from "./pages/NewsArticle";
import { Router } from "react-router-dom";
import { Route, Switch } from "react-router";
import { store } from "./redux/store";

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Layout>
            <Switch>
              <Route exact path="/news" component={News} />
              <Route path="/news/article/:id/" component={NewsArticle} />
            </Switch>
          </Layout>
        </Router>
      </Provider>
    );
  }
}

export default App;
