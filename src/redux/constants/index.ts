const requestStatus = {
  REQUEST: "_REQUEST",
  SUCCESS: "_SUCCESS",
  FAILURE: "_FAILURE",
};

const getNews = {
  GET_NEWS: "GET_NEWS",
};

const getArchives = {
  GET_ARCHIVES: "GET_ARCHIVES",
};

const getTags = {
  GET_TAGS: "GET_TAGS",
};

const comments = {
  REPLACE_COMMENTS: "REPLACE_COMMENTS",
};

const getArticle = {
  GET_ARTICLE: "GET_ARTICLE",
};

const editArticle = {
  EDIT_ARTICLE: "EDIT_ARTICLE",
};

const getSession = {
  GET_SESSION: "GET_SESSION",
};

const setSession = {
  SET_SESSION: "SET_SESSION",
};

export default {
  requestStatus,
  getNews,
  getArchives,
  comments,
  getArticle,
  editArticle,
  getTags,
  getSession,
  setSession,
};
