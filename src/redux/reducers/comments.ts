import constants from "../constants/";

const initialState = {
  isLoading: true,
  commentsDescription: undefined,
  error: undefined,
};

export const comments = (state = initialState, action) => {
  switch (action.type) {
    case constants.comments.REPLACE_COMMENTS + constants.requestStatus.REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case constants.comments.REPLACE_COMMENTS + constants.requestStatus.SUCCESS:
      return {
        isLoading: false,
        commentsDescription: action.payload,
        error: undefined,
      };
    case constants.comments.REPLACE_COMMENTS + constants.requestStatus.FAILURE:
      return {
        isLoading: false,
        commentsDescription: undefined,
        error: action.payload,
      };
    default:
      return state;
  }
};
