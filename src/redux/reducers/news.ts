import { combineReducers } from "redux";
import constants from "../constants";

const newsInitialState = {
  isLoading: true,
  newsDescription: undefined,
  error: undefined,
};

const archivesInitialState = {
  isLoading: true,
  archives: undefined,
  error: undefined,
};

const tagsInitialState = {
  isLoading: true,
  tags: undefined,
  error: undefined,
};

const news = (state = newsInitialState, action) => {
  switch (action.type) {
    case constants.getNews.GET_NEWS + constants.requestStatus.REQUEST:
      return {
        isLoading: true,
      };
    case constants.getNews.GET_NEWS + constants.requestStatus.SUCCESS:
      return {
        isLoading: false,
        newsDescription: action.payload,
      };
    case constants.getNews.GET_NEWS + constants.requestStatus.FAILURE:
      return {
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

const archives = (state = archivesInitialState, action) => {
  switch (action.type) {
    case constants.getArchives.GET_ARCHIVES + constants.requestStatus.REQUEST:
      return {
        isLoading: true,
      };
    case constants.getArchives.GET_ARCHIVES + constants.requestStatus.SUCCESS:
      return {
        isLoading: false,
        archives: action.payload,
      };
    case constants.getArchives.GET_ARCHIVES + constants.requestStatus.FAILURE:
      return {
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

const tags = (state = tagsInitialState, action) => {
  switch (action.type) {
    case constants.getTags.GET_TAGS + constants.requestStatus.REQUEST:
      return {
        isLoading: true,
      };
    case constants.getTags.GET_TAGS + constants.requestStatus.SUCCESS:
      return {
        isLoading: false,
        tags: action.payload,
      };
    case constants.getTags.GET_TAGS + constants.requestStatus.FAILURE:
      return {
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default combineReducers({
  news,
  archives,
  tags,
});
