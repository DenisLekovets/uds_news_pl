import constants from "../constants";

const initialState = {
  isLoading: null,
  data: null,
  error: null,
};

export const article = (state = initialState, action) => {
  switch (action.type) {
    case constants.getArticle.GET_ARTICLE + constants.requestStatus.REQUEST:
      return {
        isLoading: true,
      };
    case constants.getArticle.GET_ARTICLE + constants.requestStatus.SUCCESS:
      return {
        isLoading: false,
        data: action.payload,
      };
    case constants.getArticle.GET_ARTICLE + constants.requestStatus.FAILURE: {
      return {
        isLoading: false,
        error: action.error,
      };
    }
    case constants.editArticle.EDIT_ARTICLE + constants.requestStatus.SUCCESS:
      return {
        isLoading: false,
        data: action.payload,
      };
    default:
      return state;
  }
};
