import { combineReducers } from "redux";
import news from "./news";
import { comments } from "./comments";

import { article } from "./article";
import { session } from "./session";

const rootReducer = combineReducers({
  news,
  comments,
  article,
  session,
});

export default rootReducer;
