import constants from "../constants";

const sessionInitialState = {
  isLoading: true,
  session: {
    chosenTags: [],
  },
  error: undefined,
};

export const session = (state = sessionInitialState, action) => {
  switch (action.type) {
    case constants.getSession.GET_SESSION + constants.requestStatus.REQUEST:
      return {
        isLoading: true,
      };
    case constants.getSession.GET_SESSION + constants.requestStatus.SUCCESS:
      return {
        isLoading: false,
        session: JSON.parse(action.payload),
      };
    case constants.getSession.GET_SESSION + constants.requestStatus.FAILURE:
      return {
        isLoading: false,
        error: action.error,
      };

    case constants.setSession.SET_SESSION + constants.requestStatus.REQUEST:
      return {
        isLoading: true,
      };
    case constants.setSession.SET_SESSION + constants.requestStatus.SUCCESS:
      return {
        isLoading: false,
        session: JSON.parse(action.payload),
      };
    case constants.setSession.SET_SESSION + constants.requestStatus.FAILURE:
      return {
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
