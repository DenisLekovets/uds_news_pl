import news from "./news";
import comments from "./comments";
import article from "./article";
import session from "./session";

export default {
  news,
  comments,
  article,
  session,
};
