import moxios from 'moxios';
import { store } from "@uds-news/redux/store";
import actions from './';

const data = [
  {
    sample: 'text',
    another: 'property'
  },
  {
    my: 'hands',
    are: 'writing',
    unit: 'tests'
  }
];

describe('session actions', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  describe('#getSession', () => {
    it('requests the /getSession endpoint and sets the state accordingly', async () => {
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
          response: { data: JSON.stringify(data) }
        });
      });
      await actions.session.getSession()(store.dispatch);
      expect(store.getState().session.session).toStrictEqual(data);
    });
  });

  describe('#setSession', () => {
    it('requests the /setSession endpoint with the `body` argument', async () => {
      let body: any;
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        body = request.config.params;
        request.respondWith({
          status: 200,
          response: data
        });
      });

      await actions.session.setSession(data)(store.dispatch);
      expect(body).toStrictEqual({
        cmd: 'event',
        data,
        name: 'set'
      });
    });
  });
});
