import moxios from 'moxios';
import { store } from "@uds-news/redux/store";
import actions from './';

const data = [
  {
    sample: 'text',
    another: 'property'
  },
  {
    my: 'hands',
    are: 'writing',
    unit: 'tests'
  }
];

describe('comments actions', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  describe('#getComments', () => {
    it('requests the /getComments endpoint and sets the state accordingly', async () => {
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
          response: data
        });
      });
      await actions.comments.getComments(123)(store.dispatch);
      expect(store.getState().comments.commentsDescription).toStrictEqual(data);
    });
  });

  describe('#addComment', () => {
    it('requests the /addComment endpoint with the `body` argument', async () => {
      let body: any;
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        body = request.config.data;
        request.respondWith({
          status: 200,
          response: data
        });
      });

      await actions.comments.addComment(123, { name: 'me' })(store.dispatch);
      expect(JSON.parse(body)).toStrictEqual({
        name: 'me'
      });
    });
  });

  describe('#editComment', () => {
    it('requests the /editComment endpoint with the `body` argument', async () => {
      let body: any;
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        body = request.config.data;
        request.respondWith({
          status: 200,
          response: data
        });
      });

      await actions.comments.editComment(123, 123, { isLiked: false })(store.dispatch);
      expect(JSON.parse(body)).toStrictEqual({
        isLiked: false
      });
    });
  });
});
