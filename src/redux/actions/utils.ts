import constants from "../constants";

export function request(authActionType: any) {
  return { type: authActionType + constants.requestStatus.REQUEST };
}

export function success(authActionType: any, payload: any) {
  return { type: authActionType + constants.requestStatus.SUCCESS, payload };
}

export function failure(authActionType: any, error: any) {
  return { type: authActionType + constants.requestStatus.FAILURE, error };
}
