import { request, success, failure } from "./utils";
import axios from "axios";
import constants from "../constants";
import { getConfig } from "@ijl/cli";

const apiBaseUrl = getConfig()["news.api.base.url"];

function getSession() {
  return async (dispatch) => {
    dispatch(request(constants.getSession.GET_SESSION));
    await axios
      .get(apiBaseUrl + "/workflow", {
        params: {
          cmd: "start",
          data: {
            chosenTags: [],
          },
          name: "sessionFlow",
        },
        withCredentials: true
      })
      .then((res) => {
        dispatch(success(constants.getSession.GET_SESSION, res.data.data));
      })
      .catch((err) => {
        dispatch(failure(constants.getSession.GET_SESSION, err));
      });
  };
}

function setSession(data) {
  return async (dispatch) => {
    dispatch(request(constants.setSession.SET_SESSION));
    await axios
      .get(apiBaseUrl + "/workflow", {
        params: {
          cmd: "event",
          data,
          name: "set",
        },
        withCredentials: true
      })
      .then((res) => {
        dispatch(success(constants.setSession.SET_SESSION, res.data.data));
      })
      .catch((err) => {
        dispatch(failure(constants.setSession.SET_SESSION, err));
      });
  };
}

export default {
  getSession,
  setSession,
};
