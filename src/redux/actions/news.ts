import { request, success, failure } from "./utils";
import axios from "axios";
import constants from "../constants";
import { getConfig } from "@ijl/cli";

const apiBaseUrl = getConfig()["news.api.base.url"];
function getNews() {
  return async (dispatch) => {
    dispatch(request(constants.getNews.GET_NEWS));
    await axios
      .get(apiBaseUrl + "/getNews")
      .then((res) => {
        dispatch(success(constants.getNews.GET_NEWS, res.data));
      })
      .catch((err) => {
        dispatch(failure(constants.getNews.GET_NEWS, err));
      });
  };
}

function getArchives() {
  return async (dispatch) => {
    dispatch(request(constants.getArchives.GET_ARCHIVES));
    await axios
      .get(apiBaseUrl + "/getArchives")
      .then((res) => {
        dispatch(success(constants.getArchives.GET_ARCHIVES, res.data));
      })
      .catch((err) => {
        dispatch(failure(constants.getArchives.GET_ARCHIVES, err));
      });
  };
}

function getTags() {
  return async (dispatch) => {
    dispatch(request(constants.getTags.GET_TAGS));
    await axios
      .get(apiBaseUrl + "/getTags")
      .then((res) => {
        dispatch(success(constants.getTags.GET_TAGS, res.data));
      })
      .catch((err) => {
        dispatch(failure(constants.getTags.GET_TAGS, err));
      });
  };
}

export default {
  getNews,
  getArchives,
  getTags,
};
