import { request, success, failure } from "./utils";
import axios from "axios";
import constants from "../constants";
import { getConfig } from "@ijl/cli";

const apiBaseUrl = getConfig()["news.api.base.url"];

function getComments(articleId) {
  return async (dispatch) => {
    dispatch(request(constants.comments.REPLACE_COMMENTS));
    await axios
      .get(apiBaseUrl + "/getComments", {
        params: {
          id: articleId,
        },
      })
      .then((res) => {
        dispatch(success(constants.comments.REPLACE_COMMENTS, res.data));
      })
      .catch((err) => {
        dispatch(failure(constants.comments.REPLACE_COMMENTS, err));
      });
  };
}

function addComment(articleId, body) {
  return async (dispatch) => {
    dispatch(request(constants.comments.REPLACE_COMMENTS));
    try {
      const res = await axios.post(`${apiBaseUrl}/addComment`, body, {
        params: {
          id: articleId,
        },
      });
      dispatch(success(constants.comments.REPLACE_COMMENTS, res.data));
    } catch (e) {
      dispatch(failure(constants.comments.REPLACE_COMMENTS, e));
    }
  };
}

function editComment(
  articleId: number,
  commentId: number,
  body: { isLiked: boolean }
): (dispatch: any) => Promise<void> {
  return async (dispatch) => {
    dispatch(request(constants.comments.REPLACE_COMMENTS));
    try {
      const res = await axios.patch(`${apiBaseUrl}/editComment`, body, {
        params: {
          articleId,
          commentId,
        },
      });
      dispatch(success(constants.comments.REPLACE_COMMENTS, res.data));
    } catch (e) {
      dispatch(failure(constants.comments.REPLACE_COMMENTS, e));
    }
  };
}

export default {
  getComments,
  addComment,
  editComment,
};
