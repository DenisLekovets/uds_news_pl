import moxios from 'moxios';
import { store } from "@uds-news/redux/store";
import actions from './';

const data = [
  {
    sample: 'text',
    another: 'property'
  },
  {
    my: 'hands',
    are: 'writing',
    unit: 'tests'
  }
];

describe('news actions', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  describe('#getNews', () => {
    it('requests the /getNews endpoint and sets the state accordingly', async () => {
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
          response: data
        });
      });
      await actions.news.getNews()(store.dispatch);
      expect(store.getState().news.news.newsDescription).toStrictEqual(data);
    });
  });

  describe('#getArchives', () => {
    it('requests the /getArchives endpoint and sets the state accordingly', async () => {
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
          response: data
        });
      });
      await actions.news.getArchives()(store.dispatch);
      expect(store.getState().news.archives.archives).toStrictEqual(data);
    });
  });

  describe('#getTags', () => {
    it('requests the /getTags endpoint and sets the state accordingly', async () => {
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
          response: data
        });
      });
      await actions.news.getTags()(store.dispatch);
      expect(store.getState().news.tags.tags).toStrictEqual(data);
    });
  });
});
