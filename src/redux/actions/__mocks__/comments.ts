function getComments(_articleId) {
  return async (_dispatch) => {};
}

function addComment(_articleId, _body) {
  return async (_dispatch) => {};
}

function editComment(
  _articleId: number,
  _commentId: number,
  _body: { isLiked: boolean }
): (dispatch: any) => Promise<void> {
  return async (_dispatch) => {};
}

export default {
  getComments,
  addComment,
  editComment,
};
