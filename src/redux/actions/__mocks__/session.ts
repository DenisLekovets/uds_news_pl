import { success } from "../utils";
import constants from "../../constants";

function getSession() {
  return async (dispatch) => {}
}

function setSession(data) {
  return async (dispatch) => {
    dispatch(success(constants.setSession.SET_SESSION, JSON.stringify(data)));
  };
}

export default {
  getSession,
  setSession,
};
