import axios from "axios";
import constants from "../constants";
import { getConfig } from "@ijl/cli";
import { request, success, failure } from "./utils";

const apiBaseUrl = getConfig()["news.api.base.url"];

function getArticle(id: number) {
  return async (dispatch) => {
    dispatch(request(constants.getArticle.GET_ARTICLE));
    await axios
      .get(apiBaseUrl + "/getArticle", { params: { id } })
      .then((res) => {
        dispatch(success(constants.getArticle.GET_ARTICLE, res.data));
      })
      .catch((err) => {
        dispatch(failure(constants.getArticle.GET_ARTICLE, err));
      });
  };
}

function editArticle(id: number, body: { isLiked: boolean }) {
  return async (dispatch) => {
    dispatch(request(constants.editArticle.EDIT_ARTICLE));
    try {
      const res = await axios.patch(`${apiBaseUrl}/editArticle`, body, {
        params: { id },
      });
      dispatch(success(constants.editArticle.EDIT_ARTICLE, res.data));
    } catch (e) {
      dispatch(failure(constants.editArticle.EDIT_ARTICLE, e));
    }
  };
}

export default {
  getArticle,
  editArticle,
};
