import moxios from 'moxios';
import { store } from "@uds-news/redux/store";
import actions from './';

const data = {
  sample: 'text',
  another: 'property'
};

describe('article actions', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  describe('#getArticle', () => {
    it('requests the /getArticle endpoint and sets the state accordingly', async () => {
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        request.respondWith({
          status: 200,
          response: data
        });
      });
      await actions.article.getArticle(123)(store.dispatch);
      expect(store.getState().article.data).toStrictEqual(data);
    });
  });

  describe('#editArticle', () => {
    it('requests the /editArticle endpoint with the `body` argument', async () => {
      let body: any;
      moxios.wait(() => {
        const request = moxios.requests.mostRecent();
        body = request.config.data;
        request.respondWith({
          status: 200,
          response: data
        });
      });

      await actions.article.editArticle(123, { isLiked: true })(store.dispatch);
      expect(JSON.parse(body)).toStrictEqual({
        isLiked: true
      });
    });
  });
});
