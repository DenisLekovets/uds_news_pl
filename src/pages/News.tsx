import React from "react";
import "./News.scss";
import NewsList from "../components/News/NewsList/NewsList";
import { connect } from "react-redux";
import { SocialShare } from "@ijl/uds-ui";

const News = (props) => {
  const chosenTags = (props.session && props.session.chosenTags) || [];
  const chosenArchive = props.session && props.session.chosenArchive;

  let news = props.news.newsDescription;
  if (chosenTags.length !== 0) {
    news = news.filter((n) => {
      const suitable = chosenTags.every(
        (value) => n.tags.indexOf(value) !== -1
      );

      return suitable;
    });
  }


  if (chosenArchive) {
    const arch = props.archives.archives[chosenArchive];

    if (arch) {
      news = news.filter((n) => {
        const date = new Date(n.date);

        return (
          date.getMonth() === arch.month - 1 && date.getFullYear() === arch.year
        );
      });
    }
  }

  console.log(news)
  news = news.map(article => ({
    ...article,
    ncomments: article.comments.length
  }));

  return (
    <div>
      <NewsList news={news} tags={props.tags} />
      <SocialShare title={"Мы в социальных сетях"} />
    </div>
  );
};

function mapStateToProps(state) {
  return {
    archives: state.news.archives,
    news: state.news.news,
    tags: state.news.tags,
    session: state.session.session,
  };
}

export default connect(mapStateToProps)(News);
