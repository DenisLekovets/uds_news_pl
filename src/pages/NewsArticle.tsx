import React, { useEffect } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";

import "./NewsArticle.scss";
import MyText from "../components/NewsArticle/Text/Text";
import Summary from "../common/Summary/Summary";
import ShareSection from "@uds-news/components/NewsArticle/ShareSection/ShareSection";
import ClickableLike from "../common/ClickableLike/ClickableLike";
import CommentSection from "../components/NewsArticle/CommentSection/CommentSection";

import ArticleCardImage from "../../static/Rectangle 6 Copy 6.png";
import actions from "@uds-news/redux/actions";
import { PageLoader } from "@ijl/uds-ui";
import Tag from "../common/Tag/Tag";

interface Props {
  article: {
    data: {
      _id: 1;
      title: string;
      author: string;
      date: string;
      text: string;
      ncomments: number;
      comments: [];
      likes: number;
      isLiked: boolean;
      tags: any;
    };
    isLoading: boolean;
    error: string;
  };
  allTags: {
    isLoading: boolean;
    tags: Array<any>;
    error: string;
  };
  comments: any;

  getArticle(id: number): Promise<void>;
  editArticle(id: number, body: { isLiked: boolean }): Promise<void>;
}

function createArticleLike(
  article: Props["article"]["data"],
  editArticle: Props["editArticle"]
) {
  const onStatusChange = (isLiked: boolean) => {
    editArticle(article._id, { isLiked });
  };

  return (
    <ClickableLike
      likes={article.likes}
      defaultIsClicked={article.isLiked}
      onStatusChange={onStatusChange}
    />
  );
}

const NewsArticle = (props: Props) => {
  const { id } = useParams();
  const { getArticle } = props;

  useEffect(() => {
    (async function () {
      await getArticle(+id);
    })();
  }, [getArticle, id]);

  if (props.article.isLoading === null || props.article.isLoading) {
    return <PageLoader />;
  }

  const article = props.article.data;
  return (
    <div className="NewsBody">
      <h1>{article.title}</h1>
      <Summary
        parentProps={{
          author: article.author,
          date: article.date,
          ncomments: props.comments.commentsDescription?.length || 0,
          likes: article.likes,
          className: "ToLazyToCallThis",
          isLiked: article.isLiked,
        }}
      />
      <img className="ArticleCard-img" src={ArticleCardImage} alt="img" />
      <MyText parentProps={{ text: article.text }} />

      <div style={{ marginTop: 20 }}>
        {props.article.data.tags.map((id) => {
          const tag =
            props.allTags.tags &&
            props.allTags.tags.find((tag) => tag._id === id);
          return tag && <Tag key={tag._id} title={tag.text} inLine />;
        })}
      </div>

      <ShareSection />
      {createArticleLike(article, props.editArticle)}
      <CommentSection comments={article.comments} articleId={article._id} />
    </div>
  );
};

function mapStateToProps(state) {
  return {
    article: state.article,
    comments: state.comments,
    allTags: state.news.tags,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getArticle: (id: number) => dispatch(actions.article.getArticle(id)),
    editArticle: (id, body) => dispatch(actions.article.editArticle(id, body)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsArticle);
