import React, { useEffect } from "react";
import "./Layout.scss";
import NewsSider from "./common/Sider/Sider";
import { Navbar, Footer } from "@ijl/uds-ui";
import actions from "@uds-news/redux/actions";
import { connect } from "react-redux";
import { PageLoader } from "@ijl/uds-ui";

const Layout = ({
  children,
  getNews,
  getTags,
  tags,
  news,
  getArchives,
  getSession,
  archives,
}) => {
  useEffect(() => {
    getSession();
    getTags();
    getArchives();
    getNews();
  }, [getArchives, getTags, getSession, getNews]);

  const loading = tags.isLoading || news.isLoading || archives.isLoading;
  const error = tags.error || news.error || archives.error;
  return (
    <div className="News">
      <Navbar />

      {loading ? (
        <div style={{ height: "80vh" }}>
          <PageLoader />
        </div>
      ) : error ? (
        "Error"
      ) : (
        <div className={"container"}>
          <NewsSider />
          {children}
        </div>
      )}
      <Footer />
    </div>
  );
};

function mapStateToProps(state) {
  return {
    news: state.news.news,
    tags: state.news.tags,
    archives: state.news.archives,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getArchives: () => dispatch(actions.news.getArchives()),
    getNews: () => dispatch(actions.news.getNews()),
    getTags: () => dispatch(actions.news.getTags()),
    getSession: () => dispatch(actions.session.getSession()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
