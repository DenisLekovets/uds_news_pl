import Comment from "@uds-news/components/NewsArticle/Comment/Comment";
import React from "react";
import { configure, mount, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Provider } from "react-redux";
import { store } from "@uds-news/redux/store";
import { BrowserRouter } from "react-router-dom";
import Tag from "./Tag";

configure({ adapter: new Adapter() });

const Info = {
  title: "тег",
};

describe("Tag", () => {
  describe("when mounted", () => {
    const wrapper = mount(
      <Provider store={store}>
        <BrowserRouter>
          <Tag {...Info} />
        </BrowserRouter>
      </Provider>
    );

    wrapper.find('.NewsTag').simulate("click");

    it("shows title", () => {
      const p = wrapper.find(".NewsTag");
      expect(p.text()).toBe(Info.title);
      p.simulate("click");

    });
  });
});
