import React, { useState } from "react";
import "./Tag.scss";

type TagProps = {
  title: string;
  inLine?: boolean;
  clickable?: boolean;
  active?: boolean;
  onClick?: any;
};

const Tag = ({
  title,
  inLine = false,
  clickable = false,
  active,
  onClick,
}: TagProps) => {
  return (
    <div
      className={`NewsTag ${active && "NewsTag-active"} ${
        clickable && "ClickableNewsTag"
      }`}
      style={{ display: inLine ? "inline-block" : "block" }}
      onClick={() => clickable && onClick(active)}
    >
      {title}
    </div>
  );
};

export default Tag;
