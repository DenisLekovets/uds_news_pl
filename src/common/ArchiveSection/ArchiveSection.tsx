import "./ArchiveSection.scss";
import React from "react";

interface ArchiveElementProps {
  name: string;
}

function ArchiveElement(props: ArchiveElementProps) {
  return (
    <div className="ArchiveElement">
      {props.name}
      <hr />
    </div>
  );
}

export default function ArchiveSection(_props: never) {
  return (
    <div className="ArchiveSection">
      <div className="header">Архив</div>
      <hr />
      <ArchiveElement name="Март 2019" />
      <ArchiveElement name="Март 2019" />
    </div>
  );
}
