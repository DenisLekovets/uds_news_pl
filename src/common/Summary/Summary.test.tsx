import Comment from "@uds-news/components/NewsArticle/Comment/Comment";
import React from "react";
import { configure, mount, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Provider } from "react-redux";
import { store } from "@uds-news/redux/store";
import { BrowserRouter } from "react-router-dom";
import Summary from "./Summary";

configure({ adapter: new Adapter() });

const Info = {
  parentProps: {
    author: "автор",
    date: "11/22/2019",
    ncomments: 10,
    likes: 10,
    className: "className",
    isLiked: true,
  },
};

describe("Tag", () => {
  describe("when mounted", () => {
    const wrapper = mount(
      <Provider store={store}>
        <Summary {...Info} />
      </Provider>
    );

    it("shows title", () => {
      const p = wrapper.find(".summaryTest");
      expect(p.text()).toBe("автор | 22 ноября 2019 года | 10 Комментариев | ");
    });
  });
});
