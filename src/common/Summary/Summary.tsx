import React from "react";
import "./Summary.scss";
import likeBold from "../../assets/like-bold.svg";

const moment = require("moment");
moment.locale("ru");

interface Props {
  parentProps: {
    author: string;
    date: string;
    ncomments: number;
    likes: number;
    className: string;
    isLiked: boolean;
  };
}

const Summary = (propss: Props) => {
  const props = propss.parentProps;
  const set_fill = () => {
    if (props.isLiked === true) {
      return "#EE0000";
    } else {
      return "none";
    }
  };
  return (
    <div className={props.className}>
      <span className="summaryTest">
        {props.author} |{" "}
        {moment(new Date(props.date)).format("LL").replace("г.", "года")} |{" "}
        {props.ncomments} Комментариев |{" "}
      </span>
      <svg
        viewBox="0 0 40 36"
        fill={set_fill()}
        xmlns="http://www.w3.org/2000/svg"
        className="Like-icon"
      >
        {props.likes}
        <path
          id="like"
          d="M19.2977 4.17668L20 4.86957L20.7023 4.17668L20.7995 4.08082C24.9631 -0.0269393 31.7189 -0.0269395 35.8825 4.08082C40.0392 8.18164 40.0392 14.8251 35.8825 18.9259L20 34.5952L4.11746 18.9259C-0.0391543 14.8251 -0.0391544 8.18164 4.11746 4.08082C8.28111 -0.0269393 15.0369 -0.0269393 19.2005 4.08082L19.2977 4.17668Z"
          stroke="#1E1D20"
          strokeWidth="2"
        />
      </svg>
      {props.likes}
    </div>
  );
};
export default Summary;
