import React, { useState } from "react";
import "./ClickableLike.scss";

interface Props {
  likes: number;
  defaultIsClicked: boolean;
  onStatusChange?: (isLiked: boolean, likes: number) => void;
}

function ClickableLike(props: Props) {
  const [isClicked, setClicked] = useState(props.defaultIsClicked);

  const setStyle = () => {
    if (isClicked) {
      return "Like-icon Like-icon-clicked";
    } else {
      return "Like-icon Like-icon-not-clicked";
    }
  };

  const activateLike = () => {
    if (props.onStatusChange) {
      props.onStatusChange(!isClicked, props.likes);
    }
    setClicked(!isClicked);
  };

  let btn_class = setStyle();

  return (
    <div className="BigLike ClickableLike">
      <button
        style={{
          backgroundColor: "rgba(0,0,0,0)",
          outline: "none",
          border: "none",
        }}
        onClick={activateLike}
      >
        <svg
          viewBox="0 0 40 36"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          className={btn_class}
        >
          <path
            id="like"
            d="M19.2977 4.17668L20 4.86957L20.7023 4.17668L20.7995 4.08082C24.9631 -0.0269393 31.7189 -0.0269395 35.8825 4.08082C40.0392 8.18164 40.0392 14.8251 35.8825 18.9259L20 34.5952L4.11746 18.9259C-0.0391543 14.8251 -0.0391544 8.18164 4.11746 4.08082C8.28111 -0.0269393 15.0369 -0.0269393 19.2005 4.08082L19.2977 4.17668Z"
            stroke="#1E1D20"
            strokeWidth="2"
          />
        </svg>
      </button>

      <span className="like-text">{props.likes}</span>
    </div>
  );
}

export default ClickableLike;
