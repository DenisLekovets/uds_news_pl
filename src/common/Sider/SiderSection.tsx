import React from "react";

type SiderSectionProps = {
  title: string;
  children: any;
};

const SiderSection = ({ title, children }: SiderSectionProps) => {
  return (
    <div className="SiderSection">
      <div className="SiderSection-title">{title}</div>
      <div className="SiderSection-body">{children}</div>
    </div>
  );
};

export default SiderSection;
