import React from "react";
import { configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Provider } from "react-redux";
import { store } from "@uds-news/redux/store";
import { BrowserRouter } from "react-router-dom";
import Sider from "./Sider";
import { success } from "../../redux/actions/utils";
import constants from "../../redux/constants/";

jest.mock("../../redux/actions/session");
configure({ adapter: new Adapter() });

const info = {
  archives: [
    {
      _id: 0,
      title: "Archive 0",
    },
    {
      _id: 1,
      title: "Archive 1",
    }
  ],
  tags: [
    {
      _id: 0,
      text: "Tag 0",
    },
    {
      _id: 1,
      text: "Tag 1",
    }
  ],
  session: {
    chosenTags: [],
    chosenArchive: null
  }
};

describe("NewsSider", () => {
  store.dispatch(success(constants.getArchives.GET_ARCHIVES, info.archives));
  store.dispatch(success(constants.getTags.GET_TAGS, info.tags));
  store.dispatch(success(constants.getSession.GET_SESSION, JSON.stringify(info.session)));

  const wrapper = mount(
    <Provider store={store}>
      <BrowserRouter>
        <Sider />
      </BrowserRouter>
    </Provider>
  );

  describe("when mounted", () => {
    it("passes proper props to Tag", () => {
      const p = wrapper.find("Tag");
      expect(p).toHaveLength(2);
      p.forEach((element, i) => {
        expect(element.prop("title")).toBe(info.tags[i].text)
      })
    });

    it("passes proper props to Archive", () => {
      const p = wrapper.find("Archive");
      expect(p).toHaveLength(2);
      p.forEach((element, i) => {
        expect(element.prop("title")).toBe(info.archives[i].title)
      })
    });
  });

  describe("when clicked on element", () => {
    it("adds a tag to session.chosenTags when clicked on a non-active tag", () => {
      const p = wrapper.find("Tag").first();
      // @ts-ignore
      p.prop("onClick")(false);
      expect(store.getState().session.session.chosenTags).toStrictEqual([0]);
    });

    it("removes a tag from session.chosenTags when clicked on an active tag", () => {
      const p = wrapper.find("Tag").first();
      // @ts-ignore
      p.prop("onClick")(false);
      expect(store.getState().session.session.chosenTags).toStrictEqual([0]);
      // @ts-ignore
      p.prop("onClick")(true);
      expect(store.getState().session.session.chosenTags).toStrictEqual([]);
    });

    it("selects an archive when clicked on a non-active archive", () => {
      const p = wrapper.find("Archive").first();
      // @ts-ignore
      p.prop("onClick")();
      expect(store.getState().session.session.chosenArchive).toBe("" + info.archives[0]._id);
    });

    it("unselects the archive when clicked on an active archive", () => {
      let p = wrapper.find("Archive").first();
      // @ts-ignore
      p.prop("onClick")();
      expect(store.getState().session.session.chosenArchive).toBe("" + info.archives[0]._id);
      wrapper.update();
      p = wrapper.find("Archive").first();
      // @ts-ignore
      p.prop("onClick")();
      expect(store.getState().session.session.chosenArchive).toBeNull();
    });
  });
});
