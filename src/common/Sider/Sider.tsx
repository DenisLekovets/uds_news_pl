import React, { useEffect } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { PageLoader } from "@ijl/uds-ui";
import actions from "@uds-news/redux/actions";
import NewsTag from "../Tag/Tag";
import Archive from "./Archive";
import SiderSection from "./SiderSection";
import "./Sider.scss";

interface Props {
  archives: {
    isLoading: boolean;
    archives: Array<{
      _id: number;
      title: string;
    }>;
    error: string;
  };
  tags: {
    isLoading: boolean;
    tags: Array<any>;
    error: string;
  };
  session: any;
  setSession: any;

  getArchives(): Promise<void>;
  history: any;
}

const Sider = (props: Props) => {
  const chosenTags: any[] = (props.session && props.session.chosenTags) || [];
  const chosenArchive = props.session && props.session.chosenArchive;

  const chooseTag = (id: string, active: boolean) => {
    let newChosenTags: any[];
    if (active) {
      newChosenTags = chosenTags.filter((i) => i !== id);
    } else {
      newChosenTags = [...chosenTags, id];
    }

    props.setSession({ ...props.session, chosenTags: newChosenTags });
  };

  const chooseArchive = (id: number) => {
    let newChosenArchive: string | null;
    if (chosenArchive === `${id}`) {
      newChosenArchive = null;
    } else {
      newChosenArchive = `${id}`;
    }

    props.setSession({ ...props.session, chosenArchive: newChosenArchive });
  };

  const clickable =
    props.history.location.pathname.replace(/[/]/g, "") === "news";

  return (
    <div className="NewsSider">
      <SiderSection title="Тэги">
        {props.tags.tags ? (
          props.tags.tags.map((tag) => (
            <NewsTag
              key={tag._id}
              inLine
              title={tag.text}
              clickable={clickable}
              active={chosenTags.indexOf(tag._id) !== -1}
              onClick={(active) => chooseTag(tag._id, active)}
            />
          ))
        ) : (
          <PageLoader />
        )}
      </SiderSection>

      <SiderSection title="Архив">
        {props.archives.archives ? (
          props.archives.archives.map((archive, id) => (
            <Archive
              key={`arch${archive._id}`}
              title={archive.title}
              clickable={clickable}
              onClick={() => {
                chooseArchive(id);
              }}
              active={chosenArchive === `${id}`}
            />
          ))
        ) : (
          <PageLoader />
        )}
      </SiderSection>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    archives: state.news.archives,
    tags: state.news.tags,
    session: state.session.session,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setSession: (session) => dispatch(actions.session.setSession(session)),
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sider));
