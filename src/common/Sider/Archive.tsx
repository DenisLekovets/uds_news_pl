import React from "react";

type ArchiveProps = {
  title: string;
  clickable?: boolean;
  onClick?: any;
  active?: boolean;
};

const Archive = ({ title, clickable, onClick, active }: ArchiveProps) => {
  return (
    <div className={`Archive`} onClick={clickable ? onClick : () => {}}>
      <span
        style={{
          cursor: clickable && "pointer",
          color: active ? "purple" : undefined,
        }}
      >
        {title}
      </span>
    </div>
  );
};

export default Archive;
