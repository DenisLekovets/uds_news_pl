import Comment from "@uds-news/components/NewsArticle/Comment/Comment";
import React from "react";
import { configure, mount, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Provider } from "react-redux";
import { store } from "@uds-news/redux/store";

import { BrowserRouter } from "react-router-dom";
import CommentSection from "@uds-news/components/NewsArticle/CommentSection/CommentSection";

configure({ adapter: new Adapter() });
const Info = {
  articleId: 1,
  comments: [
    {
      id: 1,
      author: "Виталий Гройсман",
      date: "22 января 2019",
      text:
        "Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные? Непонятно как такое может быть? Никакого обмана!",
      likes: 13,
      isLiked: true,
    },
    {
      id: 2,
      author: "Виталий Гройсман",
      date: "22 января 2019",
      text: "Kek",
      likes: 155,
      isLiked: false,
    },
  ],
};

const commentText = "sample text comment";
const authorText = "sample text author";

describe("CommentForm", () => {
  describe("when mount", () => {
    const wrapper1 = mount(
      <Provider store={store}>
        <BrowserRouter>
          <CommentSection articleId={Info.articleId} />
        </BrowserRouter>
      </Provider>
    );
    it("shows 2 text areas fields", () => {
      const p = wrapper1.find("textarea");
      expect(p).toHaveLength(2);
    });
    it("has right labels for input fields", () => {
      const p = wrapper1.find(".form-text-comment");
      expect(p.text()).toBe("комментарий");
      const p1 = wrapper1.find(".form-text-name");
      expect(p1.text()).toBe(" имя");
    });

    it("shows inactive publish button color", () => {
      const p = wrapper1.find(".publish-button-ok");
      expect(p).toHaveLength(0);
    });
    it("shows disabled button ", () => {
      const p = wrapper1.find("button");
      expect(p.prop("disabled")).toBe(true);
    });
  });
  describe("when input is correct", () => {
    const wrapper1 = mount(
      <Provider store={store}>
        <BrowserRouter>
          <CommentSection articleId={Info.articleId} />
        </BrowserRouter>
      </Provider>
    );
    const commentField = wrapper1.find("textarea").at(0);
    commentField.simulate("change", {
      target: { value: commentText },
    });
    const authorField = wrapper1.find("textarea").at(1);
    authorField.simulate("change", {
      target: { value: authorText },
    });

    it("shows active publish button color", () => {
      const p = wrapper1.find(".publish-button-ok");
      expect(p).toHaveLength(1);
    });
    it("shows enabled button", () => {
      const p = wrapper1.find("button");
      expect(p.prop("disabled")).toBe(false);
    });
    it("shows correct texts in author and comment fields", () => {
      expect(commentField.text()).toBe(commentText);
      expect(authorField.text()).toBe(authorText);
    });
  });
});
