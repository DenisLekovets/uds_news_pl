import "./CommentForm.scss";
import React, { useState } from "react";
import actions from "@uds-news/redux/actions";
import { connect } from "react-redux";

function CommentForm(props) {
  const [text, setText] = useState("");
  const [author, setAuthor] = useState("");

  const date = "22 января 2021 года";
  const handleTextChange = (event) => {
    setText(event.target.value);
  };
  const handleNameChange = (event) => {
    setAuthor(event.target.value);
  };

  function handlePublishClick(event) {
    if (text && author) {
      event.preventDefault();
      event.stopPropagation();
      props.addComment(props.articleId, text, author, date);
    }
  }

  const setStyle = () => {
    if (text && author) {
      return "publish-button publish-button-ok";
    } else {
      return "publish-button";
    }
  };
  const setDisabled = () => {
    if (text && author) {
      return false;
    } else {
      return true;
    }
  };

  const btnClass = setStyle();
  const btnDisabled = setDisabled();
  return (
    <div className="CommentForm">
      <form onSubmit={handlePublishClick}>
        <div className="form-text-comment form-text">комментарий</div>
        <textarea
          className="comment-input"
          name="comment"
          onChange={handleTextChange}
          value={text}
        />
        <div className="form-text-name form-text"> имя</div>
        <textarea
          className="name-input"
          name="firstname"
          onChange={handleNameChange}
          value={author}
        />
        <button disabled={btnDisabled} className={btnClass}>
          {" "}
          Опубликовать
        </button>
      </form>
    </div>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    addComment: (articleId, text, author) =>
      dispatch(actions.comments.addComment(articleId, { text, author })),
  };
}

export default connect(() => ({}), mapDispatchToProps)(CommentForm);
