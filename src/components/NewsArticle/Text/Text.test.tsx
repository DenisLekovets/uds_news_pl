import { configure, mount } from "enzyme";
import React from "react";
import MyText from "@uds-news/components/NewsArticle/Text/Text";
import Adapter from "enzyme-adapter-react-16";

jest.mock("../../../redux/actions/comments");

configure({ adapter: new Adapter() });

const commentDescription = [
  {
    id: 1,
    author: "Виталий Гройсман",
    date: "22 января 2019",
    text:
      "Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные? Непонятно как такое может быть? Никакого обмана!",
    likes: 13,
    isLiked: true,
  },
  {
    id: 2,
    author: "Виталий Гройсман",
    date: "22 января 2019",
    text: "Kek",
    likes: 155,
    isLiked: false,
  },
];

const infoWithQuote = {
  id: 0,
  title:
    " ГБУ «Жилищник» стало претендентом на миллиардные контракты по капремонту",
  author: "Виталий Гройсман",
  date: "21 января 2019",
  text:
    " Вот так вот" +
    "\n" +
    "> Blockquotes are very handy in email to emulate reply text.\n" +
    "> This line is part of the same quote.",
  ncomments: 5,
  comments: commentDescription.slice(),
  likes: 10,
  isLiked: false,
};
const InfoWithoutQuote = {
  id: 0,
  title:
    " ГБУ «Жилищник» стало претендентом на миллиардные контракты по капремонту",
  author: "Виталий Гройсман",
  date: "21 января 2019",
  text: "Вот так вот",
  ncomments: 5,
  comments: commentDescription.slice(),
  likes: 10,
  isLiked: false,
};

describe("Text", () => {
  it("govno zalupi", () => {
    const textElement = mount(
      <MyText parentProps={{ text: infoWithQuote.text }} />
    );
    expect(textElement).toMatchSnapshot();
  });
});
