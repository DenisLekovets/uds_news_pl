import React from "react";
import "./Text.scss";
import MarkdownIt from "markdown-it";

interface Props {
  parentProps: {
    text: string;
  };
}

function MyText(propss: Props) {
  const props = propss.parentProps;
  const md = MarkdownIt({
    html: false,
    linkify: true,
    typographer: true,
  });
  const result = md.render(props.text);

  return (
    <div className="Text">
      <div className="text">
        <span dangerouslySetInnerHTML={{ __html: result }} />
      </div>
    </div>
  );
}

export default MyText;
