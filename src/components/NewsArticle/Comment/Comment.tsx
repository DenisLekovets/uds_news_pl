import "./Comment.scss";
import React from "react";
import actions from "@uds-news/redux/actions";
import ClickableLike from "../../../common/ClickableLike/ClickableLike";
import { connect } from "react-redux";

interface Props {
  articleId: number;
  comment: {
    _id: number;
    date: string;
    author: string;
    text: string;
    likes: number;
    isLiked: boolean;
  };

  editComment: (
    articleId: number,
    commentId: number,
    body: { isLiked: boolean }
  ) => void;
}

function createCommentLike(
  articleId: number,
  comment: Props["comment"],
  editComment: Props["editComment"]
) {
  const onStatusChange = (isLiked: boolean) => {
    editComment(articleId, comment._id, { isLiked });
  };

  return (
    <ClickableLike
      likes={comment.likes}
      defaultIsClicked={comment.isLiked}
      onStatusChange={onStatusChange}
    />
  );
}

function Comment(props: Props) {
  return (
    <div className="Comment">
      <div className="upper-comment-part">
        <span className="author">{props.comment.author}</span>
        {createCommentLike(props.articleId, props.comment, props.editComment)}
      </div>
      <div className="date">{props.comment.date}</div>
      <div className="comment-text">{props.comment.text}</div>
      <hr />
    </div>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    editComment: (articleId, commentId, body) =>
      dispatch(actions.comments.editComment(articleId, commentId, body)),
  };
}

export default connect(() => ({}), mapDispatchToProps)(Comment);
