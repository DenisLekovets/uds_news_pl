import Comment from "@uds-news/components/NewsArticle/Comment/Comment";
import React from "react";
import { configure, mount, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Provider } from "react-redux";
import { store } from "@uds-news/redux/store";
import News from "@uds-news/pages/News";
import { BrowserRouter } from "react-router-dom";

configure({ adapter: new Adapter() });
const Info = {
  articleId: 1,
  comment: {
    _id: 1,
    author: "Виталий Гройсман",
    date: "22 января 2019",
    text:
      "Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные? Непонятно как такое может быть? Никакого обмана!",
    likes: 13,
    isLiked: true,
  },
  editComment: () => {},
};

describe("Comment", () => {
  describe("when mounted", () => {
    const wrapper = mount(
      <Provider store={store}>
        <BrowserRouter>
          <Comment {...Info} />
        </BrowserRouter>
      </Provider>
    );
    it("shows text", () => {
      const p = wrapper.find(".comment-text");
      expect(p.text()).toBe(Info.comment.text);
    });
    it("shows Author", () => {
      const p = wrapper.find(".author");
      expect(p.text()).toBe(Info.comment.author);
    });
    it("shows date", () => {
      const p = wrapper.find(".date");
      expect(p.text()).toBe(Info.comment.date);
    });
    it("passes correct props to ClickableLike", () => {
      const p = wrapper.find("ClickableLike");
      expect(p.prop("likes")).toBe(13);
      expect(p.prop("defaultIsClicked")).toBeTruthy();
      expect(p.prop("onStatusChange")).toBeDefined();
    });


    it("pres ClickableLike", () => {
      const p = wrapper.find("button");
      p.simulate('click')
    });
  });
});
