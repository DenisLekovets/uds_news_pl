import { configure, mount } from "enzyme";
import { Provider } from "react-redux";
import { store } from "@uds-news/redux/store";
import React from "react";
import { BrowserRouter } from "react-router-dom";
import CommentSection from "@uds-news/components/NewsArticle/CommentSection/CommentSection";
import Adapter from "enzyme-adapter-react-16";

import constants from "../../../redux/constants/";

// TODO: refactor "export default {...}" to multiple "export function" statements
// ^ - this allows constructs like `import {success} from '.../utils'`
// ^ - which are significantly more convenient
import { success } from "../../../redux/actions/utils";

jest.mock("../../../redux/actions/comments");

configure({ adapter: new Adapter() });
const Info = {
  articleId: 1,
  comments: [
    {
      id: 1,
      author: "Виталий Гройсман",
      date: "22 января 2019",
      text:
        "Удивляют низкие цены и смущает, что множество спортивных секций и творческих кружков бесплатные? Непонятно как такое может быть? Никакого обмана!",
      likes: 13,
      isLiked: true,
    },
    {
      id: 2,
      author: "Виталий Гройсман",
      date: "22 января 2019",
      text: "Kek",
      likes: 155,
      isLiked: false,
    },
  ],
};

describe("CommentSection", () => {
  describe("when mounted", () => {
    store.dispatch(success(constants.comments.REPLACE_COMMENTS, Info.comments));
    const wrapper = mount(
      <Provider store={store}>
        <BrowserRouter>
          <CommentSection articleId={Info.articleId} />
        </BrowserRouter>
      </Provider>
    );
    it("shows right number of comments", () => {
      const p = wrapper.find("Connect(Comment)");
      expect(p).toHaveLength(2);
    });
    it("passes right props to Comment", () => {
      const p = wrapper.find("Connect(Comment)");
      p.forEach((commentElement, i) => {
        const commentArticleId = commentElement.prop("articleId");
        expect(commentArticleId).toBe(Info.articleId);
        expect(commentElement.prop("comment")).toBe(Info.comments[i]);
      });
    });
    it("passes right props to CommentForm", () => {
      const p = wrapper.find("Connect(CommentForm)");
      expect(p.prop("articleId")).toBe(Info.articleId);
    });
  });
});
