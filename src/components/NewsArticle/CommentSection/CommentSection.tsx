import "./CommentSection.scss";
import Comment from "../Comment/Comment";
import React, { useEffect } from "react";
import CommentForm from "../CommentForm/CommentForm";
import { connect } from "react-redux";
import actions from "@uds-news/redux/actions";

interface Props {
  articleId: any;
  comments?: any;

  getComments?(): Promise<void>;
}

function CommentSection(props: Props) {
  const { getComments } = props;
  useEffect(() => {
    (async () => {
      await getComments();
    })();
  }, [getComments]);

  /**
   * IMO, PageLoader should be called "krutister"
   */
  return (
    <div className="CommentSection">
      <div className="header">Комментарии</div>
      <hr />
      {props.comments.isLoading && !props.comments.commentsDescription ? (
        <div></div>
      ) : (
        props.comments.commentsDescription.map((x, index) => (
          <Comment key={index} comment={x} articleId={props.articleId} />
        ))
      )}
      <CommentForm articleId={props.articleId} />
    </div>
  );
}

function mapStateToProps(state) {
  return {
    comments: state.comments,
  };
}

function mapDispatchToProps(dispatch, ownProps: Props) {
  return {
    getComments: () =>
      dispatch(actions.comments.getComments(ownProps.articleId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentSection);
