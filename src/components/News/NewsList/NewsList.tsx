import React, { useState } from "react";
import ArticleCard, { ArticleCardProps } from "../ArticleCard";
import "./NewsList.scss";
import Paging from "../Paging";

type NewsListProps = {
  news: ArticleCardProps[];
  tags: Object[];
};

const NewsList = ({ news = [], tags }: NewsListProps) => {
  const [currentPage, setCurrentPage] = useState(1);

  return (
    <div className="NewsBody">
      {news.length ? (
        news
          .slice(5 * (currentPage - 1), 5 * currentPage)
          .map((article) => (
            <ArticleCard
              {...article}
              _id={article._id}
              key={`new${article._id}`}
            />
          ))
      ) : (
        <div className={"EmptyNews"}>Нет новостей</div>
      )}

      <Paging
        current={currentPage}
        size={5}
        count={news.length || 1}
        setCurrentPage={setCurrentPage}
      />
    </div>
  );
};

export default NewsList;
