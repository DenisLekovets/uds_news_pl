import React from "react";
import Tag from "../../common/Tag/Tag";
import { Link } from "react-router-dom";

import Summary from "../../common/Summary/Summary";
import actions from "@uds-news/redux/actions";
import { connect } from "react-redux";

export type ArticleCardProps = {
  _id: number;
  title: string;
  likes: number;
  ncomments: number;
  date: string;
  author: string;
  allTags: any;
  tags: any;
  isLiked: boolean;
};

const ArticleCard = ({
  _id,
  title,
  likes,
  ncomments,
  date,
  author,
  tags,
  allTags,
  isLiked,
}: ArticleCardProps) => {
  return (
    <div className="ArticleCard">
      <div className="ArticleCard-img"></div>

      <div className="ArticleCard-content">
        <div>
          {tags.map((id) => {
            const tag =
              allTags.tags && allTags.tags.find((tag) => tag._id === id);
            return tag && <Tag key={tag._id} title={tag.text} inLine />;
          })}
        </div>
        <div className="ArticleCard-title">
          <Link to={`/news/article/${_id}`}>{title}</Link>
        </div>
        <Summary
          parentProps={{
            author: author,
            date: date,
            ncomments: ncomments,
            likes: likes,
            className: "ArticleCard-footer",
            isLiked: isLiked,
          }}
        />
      </div>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    allTags: state.news.tags,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getTags: () => dispatch(actions.news.getTags()),
  };
}

export default connect(mapStateToProps)(ArticleCard);
