import React from "react";

type PagingProps = {
  current: number;
  count: number;
  size: number;
  setCurrentPage: (page: number) => void;
};

const Paging = ({ current, count, size, setCurrentPage }: PagingProps) => {
  return (
    <div className="Paging">
      {[...Array(Math.ceil(count / size)).keys()].map((e, i) => (
        <button
          key={`page${i}`}
          onClick={() => setCurrentPage(i + 1)}
          className={
            i === current - 1 ? "Paging-active-button" : "Paging-button"
          }
        >
          {i + 1}
        </button>
      ))}
    </div>
  );
};

export default Paging;
